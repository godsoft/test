package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

public class CustomerSpecs {

	@SuppressWarnings("serial")
	public static Specification<Customer> spec(CustomerVO vo) {
		return new Specification<Customer>() {

			@Override
			public Predicate toPredicate(Root<Customer> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> restrictions = new ArrayList<>();

				if (vo.getId() != null) {
					restrictions.add(criteriaBuilder.equal(root.get("id"), vo.getId()));
				}

				if (StringUtils.isEmpty(vo.getFirstName()) == false) {
					restrictions.add(criteriaBuilder.like(root.get("firstName"), "%" + vo.getFirstName() + "%"));
				}
				if (StringUtils.isEmpty(vo.getLastName()) == false) {
					restrictions.add(criteriaBuilder.like(root.get("lastName"), "%" + vo.getLastName() + "%"));
				}

				// query.orderBy(criteriaBuilder.desc(root.get("id")));

				return criteriaBuilder.and(restrictions.toArray(new Predicate[] {}));
			}

		};
	}

}
