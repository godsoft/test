package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

@SuppressWarnings("serial")
public class CustomerSpecification implements Specification<Customer> {

	private CustomerVO vo;

	public CustomerSpecification(CustomerVO vo) {
		this.vo = vo;
	}

	@Override
	public Predicate toPredicate(Root<Customer> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		List<Predicate> restrictions = new ArrayList<>();

		if (vo.getId() != null) {
			restrictions.add(criteriaBuilder.equal(root.get("id"), vo.getId()));
		}

		// query.orderBy(criteriaBuilder.desc(root.get("id")));

		return criteriaBuilder.and(restrictions.toArray(new Predicate[] {}));
	}

}
