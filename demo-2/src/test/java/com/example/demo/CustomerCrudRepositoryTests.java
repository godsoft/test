package com.example.demo;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerCrudRepositoryTests {

	private static final Logger log = LoggerFactory.getLogger(CustomerCrudRepositoryTests.class);

	@Autowired
	private CustomerCrudRepository repository;

	// @Test
	public void contextLoads() {
		// save();
	}

	// @Test
	public void save() {
		Customer entity = new Customer();
		entity.setFirstName("백행");
		entity.setLastName("이");
		Customer result = repository.save(entity);

		assertEquals(entity.getFirstName(), result.getFirstName());
		assertEquals(entity.getLastName(), result.getLastName());
		assertEquals(entity, result);
	}

	// @Test
	public void saveAll() {
		List<Customer> entities = new ArrayList<>();

		Customer entity = new Customer();
		entity.setFirstName("백행");
		entity.setLastName("이");
		entities.add(entity);

		entity = new Customer();
		entity.setFirstName("백행2");
		entity.setLastName("이");
		entities.add(entity);

		Iterable<Customer> results = repository.saveAll(entities);

		assertEquals(entities, results);

		entities.forEach(action -> {
			log.debug("entities=" + action);
			log.debug("id=" + action.getId());
		});

		results.forEach(action -> {
			log.debug("results=" + action);
			log.debug("id=" + action.getId());
		});
	}

	@Test
	public void findById() {
		Long id = 1L;
		Optional<Customer> result = repository.findById(id);
		log.debug("result=" + result);
		// log.debug("result=" + result.get());
		// log.debug("get=" + result.get().getId());
		// log.debug("getFirstName=" + result.get().getFirstName());
		// log.debug("getLastName=" + result.get().getLastName());
		result.ifPresent(consumer -> {
			log.debug("getId=" + consumer.getId());
			log.debug("getFirstName=" + consumer.getFirstName());
			log.debug("getLastName=" + consumer.getLastName());
		});
	}

	@Test
	public void existsById() {
		Long id = 1L;
		boolean result = repository.existsById(id);
		assertEquals(true, result);
		log.debug("result=" + result);
	}

	// @Override
	// default Iterable<Customer> findAll() {
	// // TODO Auto-generated method stub
	// return null;
	// }
	//
	// @Override
	// default Iterable<Customer> findAllById(Iterable<Long> ids) {
	// // TODO Auto-generated method stub
	// return null;
	// }
	//
	// @Override
	// default long count() {
	// // TODO Auto-generated method stub
	// return 0;
	// }
	//
	// @Override
	// default void deleteById(Long id) {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// @Override
	// default void delete(Customer entity) {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// @Override
	// default void deleteAll(Iterable<? extends Customer> entities) {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// @Override
	// default void deleteAll() {
	// // TODO Auto-generated method stub
	//
	// }

}
