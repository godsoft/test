package com.example.demo;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerRepositoryTests {

	private static final Logger log = LoggerFactory.getLogger(CustomerRepositoryTests.class);

	@Autowired
	private CustomerRepository repository;

	// @Test
	public void contextLoads() {
		CustomerVO vo = new CustomerVO();
		vo.setId(1L);

		Specification<Customer> spec = new CustomerSpecification(vo);
		// Optional<Customer> result = repository.findOne(spec);
		// log.debug("result=" + result);
		List<Customer> results = repository.findAll(spec);
		log.debug("results=" + results);
		log.debug("size=" + results.size());
	}

	// @Test
	public void findOne() {
		CustomerVO vo = new CustomerVO();
		vo.setId(1L);

		Specification<Customer> spec = new CustomerSpecification(vo);
		Optional<Customer> result = repository.findOne(spec);
		log.debug("result=" + result);
		log.debug("id=" + result.get().getId());
		log.debug("firstName=" + result.get().getFirstName());
		log.debug("lastName=" + result.get().getLastName());
	}

	// @Test
	public void findOne2() {
		CustomerVO vo = new CustomerVO();
		vo.setId(7L);
		vo.setFirstName("백행");
		vo.setLastName("이");

		Optional<Customer> result = repository.findOne(CustomerSpecs.spec(vo));

		assertEquals(vo.getId(), result.get().getId());

		log.debug("result=" + result);
		result.ifPresent(consumer -> {
			log.debug("getId=" + consumer.getId());
			log.debug("getFirstName=" + consumer.getFirstName());
			log.debug("getLastName=" + consumer.getLastName());
		});
	}

	// @Test
	public void findAll() {
		CustomerVO vo = new CustomerVO();
		// vo.setId(7L);
		vo.setFirstName("백행");
		// vo.setLastName("이");

		List<Customer> results = repository.findAll(CustomerSpecs.spec(vo));

		log.debug("results=" + results);
		log.debug("size=" + results.size());

		results.forEach(action -> {
			log.debug("getId=" + action.getId());
			log.debug("getFirstName=" + action.getFirstName());
			log.debug("getLastName=" + action.getLastName());
		});
	}

	@Test
	public void findAll2() {
		CustomerVO vo = new CustomerVO();
		// vo.setId(7L);
		vo.setFirstName("백행");
		// vo.setLastName("이");

		// Pageable pageable = PageRequest.of(0, 20);
		// Pageable pageable = PageRequest.of(0, 20, Direction.DESC, "id",
		// "firstName");
		Sort sort = Sort.by(Direction.DESC, "id");
		sort = sort.and(Sort.by(Direction.ASC, "firstName"));
		sort = sort.and(Sort.by(Direction.DESC, "lastName"));
		Pageable pageable = PageRequest.of(0, 20, sort);

		Page<Customer> results = repository.findAll(CustomerSpecs.spec(vo), pageable);

		log.debug("results=" + results);
		log.debug("getPageNumber=" + results.getPageable().getPageNumber());
		log.debug("getPageSize=" + results.getPageable().getPageSize());
		log.debug("getSort=" + results.getPageable().getSort());
		log.debug("getTotalElements=" + results.getTotalElements());

		results.forEach(action -> {
			log.debug("getId=" + action.getId());
			log.debug("getFirstName=" + action.getFirstName());
			log.debug("getLastName=" + action.getLastName());
		});
	}

}
